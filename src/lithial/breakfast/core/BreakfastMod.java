package lithial.breakfast.core;

import lithial.breakfast.proxy.CommonProxy;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Icon;
import net.minecraftforge.common.Configuration;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.PostInit;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@Mod(modid = "breakfastmod", name = "Breakfast Mod", version = "1.0")
@NetworkMod(clientSideRequired = true, serverSideRequired = true)

public class BreakfastMod {
	@SidedProxy(clientSide = "lithial.breakfast.proxy.ClientProxy", serverSide = "lithial.breakfast.proxy.CommonProxy")
	public static CommonProxy proxy;
	public static BreakfastMod instance;

	  public static  Item flour;
	  public static  Item pancakeMixedFlour;   
	  public static  Item salt;
	  public static  Item waffleMixedFlour;
	  public static  Item ironPot; 
	  public static  Item waterPot;
	  public static  Item frenchToastMixedFlour;
	  public static Item breadSlice ;
	  public static Item flouredBreadSlice;
	  public static Item bacon;
	  public static Item waffle;
	  public static Item frenchToast; 
	  public static Item pancake;  
	  public static int flourid;
	  public static int pancakeMixedFlourid;
	  public static int saltid;
	  public static int waffleMixedFlourid;
	  public static int ironPotid;
	  public static int waterPotid;
	  public static int frenchToastMixedFlourid;
	  public static int breadSliceid; 
	  public static int flouredBreadSliceid;
	  public static int waffleid;
	  public static int frenchToastid;
	  public static int pancakeid;
	  public static int baconid;
	  
 @EventHandler
public void preInit(FMLPreInitializationEvent event) {
	    Configuration config = new Configuration(event.getSuggestedConfigurationFile());

	    config.load();
	    //Items
	    flourid = config.get("Item IDs", "flour", 17000).getInt();
	    pancakeMixedFlourid = config.get("Item IDs", "pancakeMixedFlour", 17001).getInt();
	    saltid = config.get("Item IDs", "salt", 17003).getInt();
	    waffleMixedFlourid = config.get("Item IDs", "waffleMixedFlour", 17011).getInt();
	    ironPotid = config.get("Item IDs", "ironPot", 17005).getInt();
	    waterPotid = config.get("Item IDs", "waterPot", 17006).getInt();
	    frenchToastMixedFlourid = config.get("Item IDs", "frenchToastMixedFlour", 17007).getInt();
	    breadSliceid = config.get("Item IDs", "breadSlice", 17008).getInt();
	    flouredBreadSliceid = config.get("Item IDs", "flouredBreadSlice", 17009).getInt();
	    
	    //food
	    waffleid = config.get("Item IDs", "waffle", 17004).getInt();
	    frenchToastid = config.get("Item IDs", "frenchToast", 17010).getInt();
	    pancakeid = config.get("Item IDs", "pancake", 17002).getInt();
	    baconid = config.get("Item IDs" , "bacon", 17012).getInt();
	    
	    
	    config.save();
 
 

}

 @EventHandler
public void load(FMLInitializationEvent event) {
        proxy.registerRenderers();
        itemInit();
        recipes();

   	LanguageRegistry.addName(flour, "Flour");
   	LanguageRegistry.addName(pancakeMixedFlour, "Pancake Mixed Flour");
   	LanguageRegistry.addName(pancake, "Pancake");
   	LanguageRegistry.addName(salt, "salt");
   	LanguageRegistry.addName(waffleMixedFlour, "Waffle Mixed Flour");
   	LanguageRegistry.addName(waffle, "Waffle");
   	LanguageRegistry.addName(ironPot, "IronPot");
   	LanguageRegistry.addName(waterPot, "Water Pot");
   	LanguageRegistry.addName(frenchToastMixedFlour, "French Toast Mixed Flour");
   	LanguageRegistry.addName(breadSlice, "Bread Slice");
   	LanguageRegistry.addName(flouredBreadSlice, "Floured Bread Slice");
   	LanguageRegistry.addName(frenchToast, "French Toast"); 
   	LanguageRegistry.addName(bacon, "Crispy Bacon"); 
   	
}

 @EventHandler
public void postInit(FMLPostInitializationEvent event) {

}

public void recipes()
{
	GameRegistry.addRecipe(new ItemStack(pancakeMixedFlour, 3), new Object[] { "sms", "fef", "bbb", Character.valueOf('s'), Item.sugar, Character.valueOf('m'), Item.bucketMilk, Character.valueOf('b'), Item.bowlEmpty, Character.valueOf('e'), Item.egg, Character.valueOf('f'), flour });
	 GameRegistry.addRecipe(new ItemStack(waffleMixedFlour, 3), new Object[] { "sma", "fef", "bbb", Character.valueOf('s'), Item.sugar, Character.valueOf('m'), Item.bucketMilk, Character.valueOf('b'), Item.bowlEmpty, Character.valueOf('e'), Item.egg, Character.valueOf('f'), flour, Character.valueOf('a'), salt });
	 GameRegistry.addRecipe(new ItemStack(frenchToastMixedFlour, 1), new Object[] { "fsf", "bmb", "eae", Character.valueOf('s'), Item.sugar, Character.valueOf('m'), Item.bucketMilk, Character.valueOf('b'), Item.bowlEmpty, Character.valueOf('e'), Item.egg, Character.valueOf('f'), flour, Character.valueOf('a'), salt });
	 GameRegistry.addRecipe(new ItemStack(flouredBreadSlice, 8), new Object[] { "###", "#f#", "###", Character.valueOf('#'), breadSlice, Character.valueOf('f'), frenchToastMixedFlour });
	 GameRegistry.addRecipe(new ItemStack(flour, 4), new Object[] { "#", Character.valueOf('#'), Item.wheat });
	 GameRegistry.addRecipe(new ItemStack(breadSlice, 4), new Object[] { "#", Character.valueOf('#'), Item.bread });
	 GameRegistry.addRecipe(new ItemStack(ironPot, 3), new Object[] { "#", Character.valueOf('#'), Item.ingotIron });
	 GameRegistry.addRecipe(new ItemStack(waterPot, 3), new Object[] { " w ", "ppp", Character.valueOf('w'), Item.bucketWater, Character.valueOf('p'), ironPot });
	 GameRegistry.addRecipe(new ItemStack(bacon, 3), new Object[] {    "Cs", Character.valueOf('C'), Item.porkCooked, Character.valueOf('s'), salt, /*Character.valueOf('B') butter*/});

	//GameRegistry.registerItem(item, name)
	GameRegistry.addSmelting(flouredBreadSlice.itemID,new ItemStack (frenchToast, 1), 1);
	GameRegistry.addSmelting(pancakeMixedFlour.itemID,new ItemStack (pancake, 1), 1);
	GameRegistry.addSmelting(waterPot.itemID,new ItemStack (salt, 2), 1);
	GameRegistry.addSmelting(waffleMixedFlour.itemID,new ItemStack (waffle, 1), 1);

}
public void itemInit()
{
	//food
	pancake = new Itemplus( "Pancake",pancakeid, 8, 1.2F, false);
	frenchToast =  new Itemplus("FrenchToast",frenchToastid, 4, 0.6F, false);
	waffle =  new Itemplus("Waffle",waffleid, 8, 1.2F, false); 
	bacon = new Itemplus("Bacon",baconid , 10 , 1.5F , false);
//items
	flour = new basicitem("Flour", flourid, 64);
	pancakeMixedFlour = new basicitem("PancakeMixedFlour",pancakeMixedFlourid,64);  
	salt = new basicitem("Salt",saltid,64) ;
	waffleMixedFlour = new basicitem("WaffleMixedFlour",waffleMixedFlourid,64);
	ironPot = new basicitem("IronPot",ironPotid,64);
	waterPot = new basicitem("WaterPot",waterPotid,64);
	frenchToastMixedFlour = new basicitem("FrenchToastMixedFlour",frenchToastMixedFlourid,64);
	breadSlice = new basicitem("BreadSlice",breadSliceid,64);
	flouredBreadSlice = new basicitem("FlouredBreadSlice",flouredBreadSliceid,64);
 
}
@SideOnly(Side.CLIENT)
public static Icon RegisterTexture(String textureFile, IconRegister iconRegister) {
  if (!textureFile.contains(":")) {
    textureFile = "breakfastmod:" + textureFile;
  }
  if (textureFile.endsWith(".png")) {
    textureFile = textureFile.substring(0, textureFile.lastIndexOf(".png"));
  }
  return iconRegister.registerIcon(textureFile);
}
}
 