package lithial.breakfast.core;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

 

public class basicitem extends Item{
String name;
	public basicitem(String name, int id, int maxStack)
	  {
	    super(id);
	    this.name = name;

	    setUnlocalizedName(name);
	    setCreativeTab(CreativeTabs.tabFood);
	    setMaxStackSize(maxStack);
	}
    @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister par1IconRegister)
   {
       this.itemIcon = par1IconRegister.registerIcon("breakfastmod" + ":" + (this.getUnlocalizedName().substring(5)));
   }
}
 
