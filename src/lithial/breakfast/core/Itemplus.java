package lithial.breakfast.core;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemFood;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class Itemplus extends ItemFood {
	String name;
	public Itemplus( String name, int id,  int foodRestored, float sustenance, boolean wolffood)
	{
	super( id, foodRestored, sustenance, false);
	 setCreativeTab(CreativeTabs.tabFood);
	 this.name = name;
	    setUnlocalizedName(name);
	     
	}

 
    @SideOnly(Side.CLIENT)
   public void registerIcons(IconRegister par1IconRegister)
   {
       this.itemIcon = par1IconRegister.registerIcon("breakfastmod" + ":" + (this.getUnlocalizedName().substring(5)));
   }
}
 
 
 